# CPS-style Denotational Semantics for SCIL++

A formal description of the SCIL++ language through a CPS-style denotational
semantics.

---

Developed under the [FRESCO](https://release.di.ubi.pt/projects/fresco.html) project
(Formal Verification of Smart Contracts), generously funded by [Tezos
Foundation](https://tezos.foundation).
