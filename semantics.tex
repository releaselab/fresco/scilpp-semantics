\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  mathescape
}

\newcommand{\pipp}{\pi\text{++}}
\newcommand{\cond}[3]{\mathtt{cond}(#1, ~ #2, ~ #3)}
\newcommand{\fix}[1]{\mathtt{FIX} ~ #1}
\newcommand{\lookup}[2]{\mathtt{lookup} ~ #1 ~ #2}
\newcommand{\lbrackets}[1]{[\![#1]\!]}
\newcommand{\comp}[2]{\mathcal{S}\lbrackets{#1} ~ #2}
\newcommand{\config}[0]{(E_v, ~ \sigma, ~ E_l, ~ E_f)}
\newcommand{\conf}[4]{(E_v#1, ~ \sigma#2, ~ E_l#3, E_f#4)}
\newcommand{\evalexp}[2]{\mathcal{E} \lbrackets{#1} ~ #2}
\newcommand{\evalbexp}[2]{\mathcal{B} \lbrackets{#1} ~ #2}
\newcommand{\decl}[2]{\mathcal{D}_v\lbrackets{#1} ~ #2}


\begin{document}
\hypertarget{scil-softcheck-intermediate-language}{%
\section{SCIL (Softcheck Intermediate
Language)}\label{scil-softcheck-intermediate-language}}

\hypertarget{abstract-syntax}{%
\subsection{Abstract syntax}\label{abstract-syntax}}

\begin{lstlisting}
program ::=
    | <var_decl>
    | <function>
    | <program>
    | $\epsilon$

params ::=
    | <ident>
    | <ident>, <params2>
    | $\epsilon$

params2 ::=
    | <ident>
    | <ident>, <params2>

function ::=
    | FUNCTION <ident> ( params ) <instruction>

args:
    | <expr>
    | <expr>, <expr2>
    | $\epsilon$

args2:
    | <expr>
    | <expr>, <args2>

instruction ::=
    | VAR <ident>
    | <expr> := <expr>
    | IF <expr> THEN <instruction>
    | IF <expr> THEN <instruction> ELSE <instruction>
    | WHILE <expr> DO <instruction>
    | JUMP <ident>
    | LABEL <ident>
    | <instruction>; <instruction>
    | SKIP
    | BEGIN <instruction> END
    | <ident> := CALL <expr> ( args )
    | RETURN <expr>
\end{lstlisting}

\hypertarget{environments}{%
\subsection{Environments}\label{environments}}

\begin{itemize}
\item
  Location \(l \in Loc = \mathbb{Z}\)
\item
  \(new : Loc \rightarrow Loc\) function that returns the next free
  location given another location (as \(Loc\) is \(\mathbb{Z}\) we
  define as the integer successor function)
\item
  Store \(\sigma \in Store = Loc \cup \{next\} \rightarrow Val\)
  \(next\) holds the next free location
\item
  Variable environment \(env_v \in Env_V = Var \rightarrow Loc\)
\item
  Function environment \(env_f \in Env_F = x\)
\item
  \(lookup : Env_V \rightarrow Store \rightarrow Val\)
\end{itemize}

\hypertarget{configuration}{%
\subsection{Configuration}\label{configuration}}

\(State = Env_V \times Env_L \times Env_F \times Store\)

\begin{itemize}
\item
  \(c\) is the sequence of instructions to be executed
  (\(\mathbb{Z} \rightarrow statement\));
\item
  \(E\) is the variable environment \((Var \rightarrow Loc)\);
\item
  \(\sigma\) is the storage
  \((Loc \cup \{\text{ next }\} \rightarrow Val)\);
\item
  \(L\) is the label environment (\(Label \rightarrow \mathbb{Z}\)).
\end{itemize}

Note: we will write
\(\pi \mapsto \pi + 1\) as \(\pipp{}\).

\hypertarget{additional-definitions}{%
\subsection{Additional definitions}\label{additional-definitions}}

\hypertarget{denotational-semantics}{%
\subsection{Denotational semantics}\label{denotational-semantics}}

\hypertarget{auxiliary-functions}{%
\subsubsection{Auxiliary functions}\label{auxiliary-functions}}

\[
  \cond{p}{g_1}{g_2} ~ s =
  \begin{cases}
    g_1 ~ s \quad &\text{if } p ~ s = true \\
    g_2 ~ s \quad &\text{if } p ~ s = false
  \end{cases}
\]

\[ \mathtt{id} ~ x = x \]

\[ \fix{F} \]

\[ \lookup{E_v}{\sigma} = \sigma \circ E_v \]

\hypertarget{semantic-rules}{%
\subsubsection{Semantic rules}\label{semantic-rules}}

Static scope rules.

\begin{align*}
  Cont &: State \hookrightarrow State \\
  \mathcal{S} &: Stmt \rightarrow (Cont \rightarrow Cont) \\
  \mathcal{E} &: Exp \rightarrow (Var \rightarrow Val) \\
  \mathcal{B} &: Exp \rightarrow (Var \rightarrow Bool)
\end{align*}

\begin{align*}
    \decl{\mathtt{VAR} ~ x}{(E*v, \sigma)} = (E_v[x \mapsto l], ~
    \sigma[l \mapsto \bot][\text{next} ~ \mapsto \text{new} ~ l]) \\
    \text{where} ~ l = \sigma ~ \text{next}
\end{align*}

\begin{align*}
    \decl{\mathtt{VAR} ~ x := e}{(E_v, \sigma)} = (E_v[x \mapsto l], ~
    \sigma[l \mapsto v][\text{next} ~ \mapsto \text{new} ~ l]) \\
    \text{where} ~ l = \sigma ~ \text{next} \\
    \text{and} ~ v = \evalexp{e}{(\lookup{E_v}{\sigma})}
\end{align*}

\begin{align*}
    \comp{\mathtt{VAR} ~ x}{c ~ \config} = c ~ \conf{^1}{^1}{}{} \\
    \text{where} ~ (E_v^1, ~ \sigma^1) =
    \decl{\mathtt{VAR} ~ x}{(E_v, \sigma)}
\end{align*}

\begin{align*}
    \comp{\mathtt{VAR} ~ x := e}{c ~ \config} = c ~ \conf{^1}{^1}{}{} \\
    \text{where} ~ (E_v^1, ~ \sigma^1) =
    \decl{\mathtt{VAR} ~ x := e}{(E_v, \sigma)}
\end{align*}

\begin{align*}
    \comp{x := e}{c ~ \config} = c ~ \conf{}{[l \mapsto v]}{}{} \\
    \text{where } l = E_v ~ x ~ \text{and} ~ v = \evalexp{e}
    {(\lookup{E_v}{\sigma})}
\end{align*}

\begin{align*}
    \comp{\mathtt{IF} ~ e ~ \mathtt{THEN} ~ S}{c ~ \config} =
    \cond{\evalbexp{e}{(\lookup{E_v}{\sigma})}}{\comp{S}{c ~ s}}{c ~ s} \\
    \text{where } s = \config
\end{align*}

\begin{align*}
    \comp{\mathtt{IF} ~ e ~ \mathtt{THEN} ~ S_1 ~ \mathtt{ELSE} ~ S_2}{} =
    \cond{\evalbexp{e}{(\lookup{E_v}{\sigma})}}{\comp{S_1}{}}{\comp{S_2}{}}
\end{align*}

\begin{align*}
    \comp{\mathtt{WHILE} ~ e ~ \mathtt{DO} ~ S}{c ~ \config} =
    \fix{F} \\
    \text{where } (F ~ g) ~ c =
    \cond{\evalbexp{e}{(\lookup{E_v}{\sigma})}}{\comp{S}{(g ~ c)}}{c}
\end{align*}

\begin{align*} \comp{\mathtt{LABEL} ~ l}{c ~ s} = c ~ \conf{}{}{[l \mapsto c]}{} \end{align*}

\begin{align*} \comp{\mathtt{JUMP} ~ l}{c ~ \config} = (E_l ~ l) ~ \config \end{align*}

\begin{align*} \comp{S_1 \mathtt{;} S_2}{} = \comp{S_1}{} \circ ~ \comp{S_2}{} \end{align*}

\begin{align*} \comp{\mathtt{SKIP}}{} = \mathtt{id} \end{align*}

\begin{align*}
    \comp{x := \mathtt{CALL} ~ f ~ (...)}{c ~ \config} =
    c ~ \conf{}{[l \mapsto v]}{}{} \\
    \qquad \text{where } l = E_v ~ x \\
    \qquad \text{and} ~ v = \mathcal{F}\lbrackets{f ~ (e_1, ~ \dots)} ~ \config
\end{align*}

\begin{align*}
    \mathcal{F}\lbrackets{f ~ (e_1, ~ \dots)} ~ \config = v \\
    \qquad \text{where} ~ v = \sigma^1 ~ l \\
    \qquad \text{and} ~ l = \sigma ~ \text{next} \\
    \qquad \text{and} ~ \conf{^1}{^1}{^1}{^1} = \comp{S}{\text{id} ~
    \conf{'}{'}{}{}} \\
    \qquad \text{and} ~ (S, ~ [a_1, ~ \dots]) = E_f ~ f \\
    \qquad \text{and} ~ (E_v', ~ \sigma') =
    \mathcal{A}\lbrackets{([a_1, ~ \dots], ~ [e_1, ~ \dots])} ~ (E_v, \sigma)
\end{align*}

\begin{align*}
    \mathcal{A}\lbrackets{([a_1, \dots, a_n], ~ [e_1, \dots, e_n])} ~ (E_v, ~
    \sigma) = (E_v[a_1 \mapsto l_1, \dots, a_n \mapsto l_n], ~
    \sigma[l_1 \mapsto v_1, \dots, l_n \mapsto v_n]
    [\text{next} \mapsto \text{new} ~ l_n]) \\
    \qquad \text{where} ~ l_1 = \sigma ~ \text{next}, ~ \dots, ~ l_n =
    \text{new} ~ l_{n-1} \\
    \text{and} ~ v_1 = \evalexp{e_1}{(\lookup{E_v}{\sigma})}, ~ \dots, ~ v_n =
    \evalexp{e_n}{(\lookup{E_v}{\sigma})}
\end{align*}

\end{document}
